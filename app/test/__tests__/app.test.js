const request = require('supertest');

const app = require('../../../app/api');

describe("API file test", () => {
    afterEach(() => {
        app.server.close();
    });

     test("contacts API, should respond with a 200 with query parameters", () => {
        return request(app)
            .get('/apis/contacts')
            .send({ "first_name": "name", "email": "test@test.test", "company":"company name"})
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.params).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("allisterdaniels API, should respond with a 200 with no query parameters", () => {
        return request(app)
            .get('/apis/contacts/allisterdaniels')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.message).toBe("string")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("contactsByDocId API, should respond with a 200 accept docid", () => {
        return request(app)
            .get('/apis/contacts/405682d689454c639a89487b2dc9685e')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.params).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Bug1 API, should respond with a 200", () => {
        return request(app)
            .post('/apis/bug1')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(response.body.message).toBe("Bug1 called success.")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Bug2 API, should respond with a 200", () => {
        return request(app)
            .post('/apis/bug2')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(response.body.message).toBe("Bug2 called success.")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Bug3 API, should respond with a 200", () => {
        return request(app)
            .post('/apis/bug3')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(response.body.message).toBe("Bug3 called success.")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Jobs API, should respond with a 200 with query parameters", () => {
        return request(app)
            .get('/apis/jobs')
            .send({ "name": "name", "number": "9891199358", "status_name":"Active"})
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.params).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Westgate API, should respond with a 200 with query parameters", () => {
        return request(app)
            .get('/apis/jobs/westgate')
            .send({ "name": "westgate"})
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.result).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Job Search API, should respond with a 200 with query parameters", () => {
        return request(app)
            .get('/apis/jobs/search')
            .send({ "title": "React js"})
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.result).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
    test("Job Search by id API, should respond with a 200 with query parameters", () => {
        return request(app)
            .get('/apis/jobs/405682d689454c639a89487b2dc9685e')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.result).toBe("object")
            }).catch(ex =>{
                console.log("Error:", ex);
            });
    });
/*
    test("user list, should respond with a 200", () => {
        return request(app)
            .post('/user/list')
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.length > 0 ?
                    expect(response.body).toContainEqual(expect.objectContaining({ _id: expect.anything() }))
                    : expect(Array.isArray(response.body)).toBe(true);
            });
    });

    test("User create, should respond with a 200", () => {
        return request(app)
            .post('/user/create')
            .send({ "fname": "test", "lname": "Ahmad" })
            .set('Accept', /application\/json/)
            .expect('Content-Type', 'application/json; charset=utf-8')
            .expect(200)
            .then(response => {
                response.body.error ?
                    expect(response.body.error).not.toEqual('null')
                    : expect(typeof response.body.result).toBe("object")
            });
    }); */
})