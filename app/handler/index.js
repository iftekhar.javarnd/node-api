const contacts = require('./contacts');
const bugs = require('./bugs');
const jobs = require('./jobs');

module.exports = {
    contacts,
    bugs,
    jobs
}



