
let contact = (req, res) => {
    try {
        let getParams = Object.keys(req.query).length ? req.query : req.body;
        const params = Object.keys(getParams)
            .filter(key => getParams[key] && getParams[key] != 'null' && getParams[key] != null && getParams[key] != 'undefined' && getParams[key] != undefined)
            .reduce((acc, key) => Object.assign(acc, { [key]: getParams[key] }), {});

        res.status(200).json({ error: null, params: params, message: "Understand logic, test this api and review response" });
    } catch (ex) {
        res.status(200).json({ error: ex.message });
    }
};

let allisterdaniels = (req, res) => {
    try {
        res.status(200).json({ error: null, message: "Will use Bool Query as Elastic Search" });
    } catch (ex) {
        res.status(200).json({ error: ex.message });
    }
}
let contactsByDocId = (req, res) => {
    try {
        let params = req.params.hasOwnProperty('docid') ? req.params.docid : null;
        console.log("params =>", params)

        if (params && params == process.env.DOCID) return res.status(200).json({ error: null, message: "#ResourceId is matched", params: req.params });
        return res.status(200).json({ error: null, message: "#ResourceId is mismatched", params: req.params });
    } catch (ex) {
        res.status(200).json({ error: ex.message });
    }
}
let contactsbug = (req, res) => {
    try {
        res.status(200).json({ error: null, message: "You are authorized" });
    } catch (ex) {
        res.status(200).json({ error: ex.message });
    }
}

module.exports = {
    contact,
    allisterdaniels,
    contactsByDocId,
    contactsbug
};