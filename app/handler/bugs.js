
const errorMsg = "There was an error processing your request.";

let bug1 = (req, res) => {
    try {
        res.status(200).json({ error:null, message: "Bug1 called success."});
    } catch (ex) {
        res.status(500).json({ error: ex.message, message: errorMsg});
    }
}
let bug2 = (req, res) => {
    try {
        res.status(200).json({ error:null, message: "Bug2 called success."});
    } catch (ex) {
        res.status(500).json({ error: ex.message, message: errorMsg});
    }
}
let bug3 = (req, res) => {
    try {
        res.status(200).json({ error:null, message: "Bug3 called success."});
    } catch (ex) {
        res.status(500).json({ error: ex.message, message: errorMsg});
    }
}
module.exports = {
    bug1,
    bug2,
    bug3
};