let objWestgate = [
    {
        name:"westgate",
        number:9891199358,
        status_name:"Active"   
    },
    {
        name:"westgate 1",
        number:9891199356,
        status_name:"Active"   
    },
    {
        name:"westgate 2",
        number:9891199358,
        status_name:"Inactive"   
    }
]
let jobList = [
    {
        _id:'405682d689454c639a89487b2dc9685e',
        title:"Nodejs Backend Developer",
        number:9891199358,
        status_name:"Active"   
    },
    {
        _id:'405682d689454c639a89487b2dc9685f',
        title:"React js UI Developer",
        number:9891199353,
        status_name:"Active"   
    },
    {
        _id:'405682d689454c639a89487b2dc9685g',
        title:"React js Frontend Developer",
        number:9891199321,
        status_name:"Active"   
    },
]

let getJobs = (req, res) => {
    try {
        let getParams = Object.keys(req.query).length ? req.query : req.body;
        const params = Object.keys(getParams)
        .filter(key => getParams[key] && getParams[key] != 'null' && getParams[key] != null && getParams[key] != 'undefined' && getParams[key] != undefined)
        .reduce((acc, key) => Object.assign(acc, { [key]: req.query[key] }), {});

        res.status(200).json({ error: null, params: params, message: "Jobs end point is called"});
    } catch (ex) {
        res.status(500).json({ error: ex.message });
    }
}
let westgate = (req, res) => {
    try {
        let name = Object.keys(req.query).length ? req.query.name : req.body.name;
        let obj = objWestgate.find(item =>{ return item.name === name});
        if(obj) return res.status(200).json({error:null, result:obj, message:"Record exists"})
        return res.status(200).json({error:null, result:{}, message:"Record does not exists"})
    } catch (ex) {
        res.status(500).json({ error: ex.message });
    }
}
let search = (req, res) => {
    try {
        let title = Object.keys(req.query).length ? req.query.title : req.body.title;
        let result = jobList.filter(obj=>obj.title.includes(title));    
        if(result.length) return res.status(200).json({ error: null, result:result, message:"Record exists"});
        return res.status(200).json({ error: null, result:result, message:"Record does not exists"});
    } catch (ex) {
        res.status(500).json({ error: ex.message });
    }
}
let jobsByDocId = (req, res) => {
    try {
        let docid = req.params.hasOwnProperty('docid') ? req.params.docid:null; 
        let result = jobList.find(item =>{ return item._id ==docid})
        if(result) return res.status(200).json({ error: null, result:result, message:"Record exists"});
        return res.status(200).json({ error: null, result:{}, message:"Record does not exists"});
    } catch (ex) {
        res.status(500).json({ error: ex.message });
    }
}

module.exports = {
    getJobs,
    westgate,
    search,
    jobsByDocId    
};