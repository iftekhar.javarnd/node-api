
module.exports = (req, res, next) =>{

    // Get header info
    let token = req.headers.hasOwnProperty('authorization') ? req.headers.authorization.split(" ")[1]:null;
    if(token){
        if(token === process.env.TOKEN)
        {
            return next();
        }else{
            return res.status(401).json({message:"Token invalid"})
        }
    } else{
        return res.status(400).json({message:"Token is missing"})
    }
}