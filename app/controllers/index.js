const handler = require('../handler')

// Contacts handlers
exports.contacts = (req, res) =>{
    handler.contacts.contact(req, res);
}
exports.allisterdaniels = (req, res) =>{
    handler.contacts.allisterdaniels(req, res);
}
exports.contactsByDocId = (req, res) =>{
    handler.contacts.contactsByDocId(req, res);
}
exports.contactsbug = (req, res) =>{
    handler.contacts.contactsbug(req, res);
}
// Bugs handlers
exports.bug1 = (req, res) =>{
    handler.bugs.bug1(req, res);
}
exports.bug2 = (req, res) =>{
    handler.bugs.bug2(req, res);
}
exports.bug3 = (req, res) =>{
    handler.bugs.bug3(req, res);
}
// Jobs handlers
exports.jobs = (req, res) =>{
    handler.jobs.getJobs(req, res);
}
exports.westgate = (req, res) =>{
    handler.jobs.westgate(req, res);
}
exports.search = (req, res) =>{
    handler.jobs.search(req, res);
}
exports.jobsByDocId = (req, res) =>{
    handler.jobs.jobsByDocId(req, res);
}
