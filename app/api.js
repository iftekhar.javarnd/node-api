require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
let app = express();
let routes = require('./route');
let middleware = require('./middleware/token-authenticate');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/apis/contactsbug', middleware);
app.use('/apis', routes);
// server
const port = process.env.PORT || 4000;
app.server = app.listen(port);
console.log(`listening on port ${port}`);

module.exports = app;