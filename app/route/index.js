const routes = require('express').Router();
const controllers = require('../controllers');

// Define route

// Contacts routing
routes.get('/contacts', (req, res) =>{
    controllers.contacts(req, res);
});
routes.get('/contacts/allisterdaniels', (req, res) =>{
    controllers.allisterdaniels(req, res);
});
routes.get('/contacts/:docid', (req, res) =>{
    controllers.contactsByDocId(req, res);
});
routes.get('/contactsbug', (req, res) =>{
    controllers.contactsbug(req, res);
});
// Bugs routing
routes.post('/bug1', (req, res) =>{
    controllers.bug1(req, res);
});
routes.post('/bug2', (req, res) =>{
    controllers.bug2(req, res);
});
routes.post('/bug3', (req, res) =>{
    controllers.bug3(req, res);
});
// Jobs routing
routes.get('/jobs', (req, res) =>{
    controllers.jobs(req, res);
});
routes.get('/jobs/westgate', (req, res) =>{
    controllers.westgate(req, res);
});
routes.get('/jobs/search', (req, res) =>{
    controllers.search(req, res);
});
routes.get('/jobs/:docid', (req, res) =>{
    controllers.jobsByDocId(req, res);
});

module.exports = routes;