node api exercise
======================================

### Development build
In application root path run following commands
<pre>
cd node-api
npm install
npm start
</pre>

### Testing
Run following commands
<pre>
npm test
</pre>

## Environment variables
Environment variables is the main mechanism of manipulating application settings. Currently application recognizes
following environment variables:
<pre>
PORT=3000
TOKEN=dfmgdigudfjdf
DOCID=405682d689454c639a89487b2dc9685e
</pre>